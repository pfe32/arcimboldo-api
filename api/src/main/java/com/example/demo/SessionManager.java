package com.example.demo;

import com.example.demo.model.User;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SessionManager {
    private Map<String, User> sessions;

    public SessionManager() {
        this.sessions = new HashMap<>();
    }

    /**
     * Logs the user in (creates a session).
     * @param user User to log in.
     * @return Session token to send to the user.
     */
    public String newSession(User user) {
        String token = UUID.randomUUID().toString();
        sessions.put(token, user);
        return token;
    }

    /**
     * Returns a logged in user.
     * @param sessionToken Session token provided by the user.
     * @return The logged in user corresponding to the given token, or null if it is not logged in.
     */
    public User getUser(String sessionToken) {
        return this.sessions.get(sessionToken);
    }
}
