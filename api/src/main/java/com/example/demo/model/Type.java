package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.DayOfWeek;
import java.util.*;

@Table(name = "Type")
@Entity
public class Type implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "idType", nullable = false)
    private Integer idType;

    @Column(name = "nameType")
    private String nameType;


    @ManyToOne
    private ParentCategorie parentCategorie;

    @OneToMany(
            mappedBy = "type",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnoreProperties("type")
    private List<TypeHasRecipe> recipes = new ArrayList<>();

    @OneToMany(mappedBy = "type", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnoreProperties("type")
    private List<Product> products = new ArrayList<Product>();

    public Type() {
    }

    public Type(String nameType, ParentCategorie parentCategorie) {
        this.nameType = nameType;
        this.parentCategorie = parentCategorie;
    }

    public Integer getIdType() {
        return idType;
    }

    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    public ParentCategorie getParentCategorie() {
        return parentCategorie;
    }

    public void setParentCategorie(ParentCategorie parentCategorie) {
        this.parentCategorie = parentCategorie;
    }

    public List<TypeHasRecipe> getRecipes() {
        return recipes;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    /**
     * Returns the availability of the type at a given location.
     * @param latitude
     * @param longitude
     * @param quantity Wanted quantity
     * @return IDs of markets the type is available at, associated with the days it is available.
     */
    public Map<Integer, Set<DayOfWeek>> availability(double latitude, double longitude, Float quantity) {
        Map<Integer, Set<DayOfWeek>> availability = new HashMap<>(); // Markets and days the type is available
        // Add all the markets days this type is available close to the given coordinates
        for (Product product : this.getProducts()) {
            for (Map.Entry<Integer, Set<DayOfWeek>> productMarketAvailability : product.availability(latitude, longitude, quantity).entrySet()) {
                if (!availability.containsKey(productMarketAvailability.getKey())) {
                    availability.put(productMarketAvailability.getKey(), productMarketAvailability.getValue());
                } else {
                    availability.get(productMarketAvailability.getKey()).addAll(productMarketAvailability.getValue());
                }
            }
        }
        return availability;
    }
}