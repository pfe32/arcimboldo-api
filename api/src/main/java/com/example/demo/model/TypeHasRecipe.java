package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Type_has_Recipe")
public class TypeHasRecipe implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private TypeHasRecipeId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("typeId")
    @JsonIgnoreProperties("recipes")
    private Type type;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("recipeId")
    @JsonIgnoreProperties("types")
    private Recipe recipe;

    @Column(name = "quantity", nullable = false)
    private Float quantity;

    public TypeHasRecipe() {
    }

    public TypeHasRecipe(Type type, Recipe recipe, Float quantity) {
        this.type = type;
        this.recipe = recipe;
        this.quantity = quantity;
        this.id = new TypeHasRecipeId(type.getIdType(), recipe.getIdRecipe());
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }
}