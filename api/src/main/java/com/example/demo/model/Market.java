package com.example.demo.model;

import com.example.demo.DemoApplication;
import com.example.demo.Util;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Table(name = "Market")
@Entity
public class Market implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "idMarket", nullable = false)
    private Integer idMarket;


    @OneToMany(mappedBy = "market", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    private List<Order> orders = new ArrayList<Order>();

    @OneToOne
    private Address address;

    @OneToMany(
        mappedBy = "market",
        cascade = CascadeType.ALL,
        orphanRemoval = true
    )
    @JsonIgnoreProperties("market")
    private List<MarketDay> marketDays = new ArrayList<>();

    public Market() {
    }

    public Market(Address address) {
        this.address = address;
    }

    public Integer getIdMarket() {
        return idMarket;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<MarketDay> getMarketDays() {
        return marketDays;
    }

    /**
     * Returns true if the given point is close enough to this market and can get food.
     * @param latitude Latitude of the point
     * @param longitude Longitude of the point
     * @return
     */
    public boolean isCloseTo(double latitude, double longitude)
    {
        return Util.distance(this.address.getLatitudeAddress(), this.address.getLongitudeAddress(), latitude, longitude) <= DemoApplication.MAX_MARKET_DISTANCE;
    }
}