package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Table(name = "User")
@Entity
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idUser", insertable = false, nullable = false)
    private Integer idUser;

    @Column(name = "emailUser", nullable = false)
    private String emailUser;

    @Column(name = "surnameUser")
    private String surnameUser;

    @Column(name = "passwordUser")
    @JsonIgnore
    private String passwordUser;

    @Column(name = "birthdayUser")
    private Date birthdayUser;


    @OneToMany(mappedBy="user", cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Order> orders = new ArrayList<Order>();

    @OneToMany(mappedBy="userRecipe", cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Recipe> recipe;

    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnoreProperties("user")
    private List<OrderedRecipe> orderedRecipes = new ArrayList<>();

    public User() {
    }

    public User(String emailUser, String surnameUser, String passwordUser) {
        this.emailUser = emailUser;
        this.surnameUser = surnameUser;
        this.passwordUser = passwordUser;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public String getEmailUser() {
        return emailUser;
    }

    public void setEmailUser(String emailUser) {
        this.emailUser = emailUser;
    }

    public String getSurnameUser() {
        return surnameUser;
    }

    public void setSurnameUser(String surnameUser) {
        this.surnameUser = surnameUser;
    }

    public String getPasswordUser() {
        return passwordUser;
    }

    public void setPasswordUser(String passwordUser) {
        this.passwordUser = passwordUser;
    }

    public Date getBirthdayUser() {
        return birthdayUser;
    }

    public void setBirthdayUser(Date birthdayUser) {
        this.birthdayUser = birthdayUser;
    }
}