package com.example.demo.model;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class OrderedRecipeId implements Serializable {
    private Integer userId;
    private Integer recipeId;

    public OrderedRecipeId() {
    }
    public OrderedRecipeId(Integer userId, Integer recipeId) {
        this.userId = userId;
        this.recipeId = recipeId;
    }
}
