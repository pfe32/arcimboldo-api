package com.example.demo.model;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class TypeHasRecipeId implements Serializable {
    private Integer typeId;
    private Integer recipeId;

    public TypeHasRecipeId() {
    }
    public TypeHasRecipeId(Integer typeId, Integer recipeId) {
        this.typeId = typeId;
        this.recipeId = recipeId;
    }
}