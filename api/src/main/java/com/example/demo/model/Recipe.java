package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.DayOfWeek;
import java.util.*;

@Table(name = "Recipe")
@Entity
public class Recipe implements Serializable {
    private static final long serialVersionUID = 1L;

    public enum TypeRecipe {
        RECIPE,
        CUSTOM
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "idRecipe", nullable = false)
    private Integer idRecipe;

    @Column(name = "nameRecipe")
    private String nameRecipe;

    @Column(name = "durationRecipe")
    private Integer durationRecipe;

    @Column(name = "descriptionRecipe")
    private String descriptionRecipe;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "ENUM('RECIPE', 'CUSTOM')", nullable = false)
    private TypeRecipe typeRecipe;

    private String imageUrlRecipe;

    @ManyToOne
    private User userRecipe;

    @OneToMany(
            mappedBy = "recipe",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnoreProperties("recipe")
    private List<TypeHasRecipe> types = new ArrayList<>();

    @OneToMany(
            mappedBy = "recipe",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnoreProperties("recipe")
    private List<OrderedRecipe> orderedByUsers = new ArrayList<>();

    public Recipe() {
    }

    public Recipe(String nameRecipe, Integer durationRecipe, String descriptionRecipe, TypeRecipe typeRecipe, User userRecipe) {
        this.nameRecipe = nameRecipe;
        this.durationRecipe = durationRecipe;
        this.descriptionRecipe = descriptionRecipe;
        this.typeRecipe = typeRecipe;
        this.userRecipe = userRecipe;
    }

    public Integer getIdRecipe() {
        return idRecipe;
    }

    public void setIdRecipe(Integer idRecipe) {
        this.idRecipe = idRecipe;
    }

    public String getNameRecipe() {
        return nameRecipe;
    }

    public void setNameRecipe(String nameRecipe) {
        this.nameRecipe = nameRecipe;
    }

    public Integer getDurationRecipe() {
        return durationRecipe;
    }

    public void setDurationRecipe(Integer durationRecipe) {
        this.durationRecipe = durationRecipe;
    }

    public String getDescriptionRecipe() {
        return descriptionRecipe;
    }

    public void setDescriptionRecipe(String descriptionRecipe) {
        this.descriptionRecipe = descriptionRecipe;
    }

    public TypeRecipe getTypeRecipe() {
        return typeRecipe;
    }

    public void setTypeRecipe(TypeRecipe typeRecipe) {
        this.typeRecipe = typeRecipe;
    }

    public User getUserRecipe() {
        return userRecipe;
    }

    public void setUserRecipe(User userRecipe) {
        this.userRecipe = userRecipe;
    }

    public List<TypeHasRecipe> getTypes() {
        return types;
    }

    public void setTypes(List<TypeHasRecipe> types) {
        this.types = types;
    }

    public String getImageUrlRecipe() {
        return imageUrlRecipe;
    }

    public void setImageUrlRecipe(String imageUrlRecipe) {
        this.imageUrlRecipe = imageUrlRecipe;
    }

    public void addType(Type type, Float quantity) {
        TypeHasRecipe typeHasRecipe = new TypeHasRecipe(type, this, quantity);
        types.add(typeHasRecipe);
        type.getRecipes().add(typeHasRecipe);
    }

    public void removeType(Type type) {
        for (Iterator<TypeHasRecipe> iterator = types.iterator(); iterator.hasNext(); ) {
            TypeHasRecipe typeHasRecipe = iterator.next();

            if (typeHasRecipe.getRecipe().equals(this) && typeHasRecipe.getType().equals(type)) {
                iterator.remove();
                typeHasRecipe.getType().getRecipes().remove(typeHasRecipe);
                typeHasRecipe.setRecipe(null);
                typeHasRecipe.setType(null);
            }
        }
    }

    /**
     * Returns the availability of the recipe at a given location.
     * @param latitude
     * @param longitude
     * @return IDs of markets the recipe is available at, associated with the days it is available.
     */
    public Map<Integer, Set<DayOfWeek>> availability(double latitude, double longitude) {
        Map<Integer, Set<DayOfWeek>> availability = new HashMap<>(); // Markets and days the recipe is available
        boolean first = true;
        for (TypeHasRecipe ingredient : this.types) {
            Map<Integer, Set<DayOfWeek>> ingredientAvailability = ingredient.getType().availability(latitude, longitude, ingredient.getQuantity());
            // Fill availability with market days the first ingredient is available
            if (first) {
                first = false;
                availability = ingredientAvailability;
            } else {
                // Remove from availability markets and days this ingredient is not available
                Iterator<Map.Entry<Integer, Set<DayOfWeek>>> marketIterator = availability.entrySet().iterator();
                while (marketIterator.hasNext()) {
                    Map.Entry<Integer, Set<DayOfWeek>> marketAvailability = marketIterator.next();
                    // Remove market if the ingredient is not available in it
                    if (!ingredientAvailability.containsKey(marketAvailability.getKey())) {
                        marketIterator.remove();
                    } else {
                        Iterator<DayOfWeek> dayIterator = marketAvailability.getValue().iterator();
                        while (dayIterator.hasNext()) {
                            DayOfWeek day = dayIterator.next();
                            // Remove day if the ingredient is not available during it
                            if (!ingredientAvailability.get(marketAvailability.getKey()).contains(day)) {
                                dayIterator.remove();
                            }
                        }
                        // Remove markets with no available days
                        if (marketAvailability.getValue().isEmpty())
                            marketIterator.remove();
                    }
                }
            }
        }
        return availability;
    }
}