package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Producer")
public class Producer implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idProducer", insertable = false, nullable = false)
    private Integer idProducer;

    @Column(name = "nameProducer")
    private String nameProducer;

    @Column(name = "emailContact")
    private String emailContact;

    @Column(name = "phoneNumber")
    private Long phoneNumber;


    @OneToMany(mappedBy="producer", cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Product> products = new ArrayList<Product>();

    @ManyToMany(mappedBy="producers")
    private List<Address> addresses = new ArrayList<Address>();

    @OneToMany(
        mappedBy = "producer",
        cascade = CascadeType.ALL,
        orphanRemoval = true
    )
    @JsonIgnoreProperties("producer")
    private List<MarketDay> marketDays = new ArrayList<>();


    public Producer() {
    }

    public Producer(String nameProducer, String emailContact, Long phoneNumber, List<Address> addresses) {
        this.nameProducer = nameProducer;
        this.emailContact = emailContact;
        this.phoneNumber = phoneNumber;
        this.addresses = addresses;
    }

    public Integer getIdProducer() {
        return idProducer;
    }

    public String getNameProducer() {
        return nameProducer;
    }

    public void setNameProducer(String nameProducer) {
        this.nameProducer = nameProducer;
    }

    public String getEmailContact() {
        return emailContact;
    }

    public void setEmailContact(String emailContact) {
        this.emailContact = emailContact;
    }

    public Long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<MarketDay> getMarketDays() {
        return marketDays;
    }

    public void setMarketDays(List<MarketDay> marketDays) {
        this.marketDays = marketDays;
    }
}