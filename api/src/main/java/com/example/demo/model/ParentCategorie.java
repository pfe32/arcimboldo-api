package com.example.demo.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ParentCategorie")
public class ParentCategorie implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "idParentCategorie", nullable = false)
    private Integer idParentCategorie;

    @Column(name = "nameParentCategorie")
    private String nameParentCategorie;


    @OneToMany(mappedBy="parentCategorie", cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Type> types = new ArrayList<Type>();
}