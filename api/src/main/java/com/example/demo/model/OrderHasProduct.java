package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "Order_has_Product")
@Entity
public class OrderHasProduct implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private OrderHasProductId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("orderId")
    @JsonIgnoreProperties("products")
    private Order order;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("productId")
    @JsonIgnoreProperties("orders")
    private Product product;

    @Column(name = "quantity", nullable = false)
    private Float quantity;

    public OrderHasProduct() {
    }

    public OrderHasProduct(Order order, Product product, Float quantity) {
        this.order = order;
        this.product = product;
        this.quantity = quantity;
        this.id = new OrderHasProductId(order.getIdOrder(), product.getIdProduct());
    }

    public Order getOrder() {
        return order;
    }

    public Product getProduct() {
        return product;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }
}