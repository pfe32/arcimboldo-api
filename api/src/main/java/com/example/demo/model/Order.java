package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Table(name = "Orders")
@Entity
public class Order implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idOrder", insertable = false, nullable = false)
    private Integer idOrder;

    @Column(name = "totalPriceOrder")
    private Float totalPriceOrder;

    @Column(name = "deliveryMode")
    private String deliveryMode;

    @Column(name = "marketDateOrder")
    private Date marketDateOrder;


    @OneToMany(
        mappedBy = "order",
        cascade = CascadeType.ALL,
        orphanRemoval = true
    )
    @JsonIgnoreProperties("order")
    private List<OrderHasProduct> products = new ArrayList<>();

    @ManyToOne
    private User user;

    @ManyToOne
    private Market market;

    @ManyToOne
    private Address address;

    public Order() {
    }

    public Order(Float totalPriceOrder, String deliveryMode, Date marketDateOrder, User user, Market market, Address address) {
        this.totalPriceOrder = totalPriceOrder;
        this.deliveryMode = deliveryMode;
        this.marketDateOrder = marketDateOrder;
        this.user = user;
        this.market = market;
        this.address = address;
    }

    public Integer getIdOrder() {
        return idOrder;
    }

    public Float getTotalPriceOrder() {
        return totalPriceOrder;
    }

    public void setTotalPriceOrder(Float totalPriceOrder) {
        this.totalPriceOrder = totalPriceOrder;
    }

    public String getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public Date getMarketDateOrder() {
        return marketDateOrder;
    }

    public void setMarketDateOrder(Date marketDateOrder) {
        this.marketDateOrder = marketDateOrder;
    }

    public List<OrderHasProduct> getProducts() {
        return products;
    }

    public void setProducts(List<OrderHasProduct> products) {
        this.products = products;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}