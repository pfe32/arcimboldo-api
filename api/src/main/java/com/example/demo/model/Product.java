package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.DayOfWeek;
import java.util.*;

@Entity
@Table(name = "Product")
public class Product implements Serializable {
    private static final long serialVersionUID = 1L;

    public enum UnitProduct {
        KG,
        PIECE
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idProduct", insertable = false, nullable = false)
    private Integer idProduct;

    @Column(name = "nameProduct")
    private String nameProduct;

    @Column(name = "descriptionProduct")
    private String descriptionProduct;

    @Column(name = "amountProduct")
    private Double amountProduct;

    @Column(name = "priceProduct")
    private Float priceProduct;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "ENUM('KG', 'PIECE')")
    private UnitProduct unitProduct;

    private String imageUrlProduct;


    @OneToMany(
            mappedBy = "product",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnoreProperties("product")
    @JsonIgnore
    private List<OrderHasProduct> orders = new ArrayList<>();

    @ManyToOne
    private Type type;

    @ManyToOne
    @JsonIgnoreProperties("products")
    private Producer producer;

    public Product() {
    }

    public Product(String nameProduct, String descriptionProduct, Double amountProduct, Float priceProduct, UnitProduct unitProduct, Type type, Producer producer) {
        this.nameProduct = nameProduct;
        this.descriptionProduct = descriptionProduct;
        this.amountProduct = amountProduct;
        this.priceProduct = priceProduct;
        this.unitProduct = unitProduct;
        this.type = type;
        this.producer = producer;
    }

    public Integer getIdProduct() {
        return idProduct;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public String getDescriptionProduct() {
        return descriptionProduct;
    }

    public void setDescriptionProduct(String descriptionProduct) {
        this.descriptionProduct = descriptionProduct;
    }

    public Double getAmountProduct() {
        return amountProduct;
    }

    public void setAmountProduct(Double amountProduct) {
        this.amountProduct = amountProduct;
    }

    public Float getPriceProduct() {
        return Math.round(priceProduct * 100f) / 100f;
    }

    public void setPriceProduct(Float priceProduct) {
        this.priceProduct = priceProduct;
    }

    public UnitProduct getUnitProduct() {
        return unitProduct;
    }

    public void setUnitProduct(UnitProduct unitProduct) {
        this.unitProduct = unitProduct;
    }

    public List<OrderHasProduct> getOrders() {
        return orders;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public String getImageUrlProduct() {
        return imageUrlProduct;
    }

    public void setImageUrlProduct(String imageUrlProduct) {
        this.imageUrlProduct = imageUrlProduct;
    }

    /**
     * Returns the quantity of product available to order.
     * @return Total stock minus the already ordered quantity.
     */
    public double getAvailableQuantity() {
        double quantity = this.getAmountProduct();
        Date currentDate = new Date();
        for (OrderHasProduct order : this.getOrders()) {
            if (order.getOrder().getMarketDateOrder().after(currentDate)) {
                quantity -= order.getQuantity();
            }
        }
        return Math.round(quantity * 1000.0) / 1000.0;
    }

    /**
     * Returns the availability of the product at a given location.
     * @param latitude
     * @param longitude
     * @param quantity Wanted quantity of product
     * @return IDs of markets the product is available at, associated with the days it is available.
     */
    public Map<Integer, Set<DayOfWeek>> availability(double latitude, double longitude, Float quantity) {
        Map<Integer, Set<DayOfWeek>> availability = new HashMap<>(); // Markets and days the product is available
        if (this.getAvailableQuantity() >= quantity) {
            // Add all the markets days this product is available close to the given coordinates
            for (MarketDay marketDay : this.getProducer().getMarketDays()) {
                if (marketDay.getMarket().isCloseTo(latitude, longitude)) {
                    if (!availability.containsKey(marketDay.getMarket().getIdMarket())) {
                        availability.put(marketDay.getMarket().getIdMarket(), new HashSet<>());
                    }
                    availability.get(marketDay.getMarket().getIdMarket()).add(marketDay.getDayMarketDay());
                }
            }
        }
        return availability;
    }
}