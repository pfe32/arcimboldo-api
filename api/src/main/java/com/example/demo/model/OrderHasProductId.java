package com.example.demo.model;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class OrderHasProductId implements Serializable {
    private Integer orderId;
    private Integer productId;

    public OrderHasProductId() {
    }

    public OrderHasProductId(Integer orderId, Integer productId) {
        this.orderId = orderId;
        this.productId = productId;
    }
}
