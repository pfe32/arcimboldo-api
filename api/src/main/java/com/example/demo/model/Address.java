package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Table(name = "Address")
@Entity
public class Address implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "idAddress", nullable = false)
    private Integer idAddress;

    @Column(name = "streetAddress")
    private String streetAddress;

    @Column(name = "cityAddress")
    private String cityAddress;

    @Column(name = "countryAddress")
    private String countryAddress;

    @Column(name = "zipCodeAddress")
    private Integer zipCodeAddress;

    @Column(name = "latitudeAddress")
    private Double latitudeAddress;

    @Column(name = "longitudeAddress")
    private Double longitudeAddress;


    @OneToOne(mappedBy = "address")
    private Market market;

    @ManyToMany
    private List<Producer> producers = new ArrayList<Producer>();

    @OneToMany(mappedBy = "address", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonIgnore
    private List<Order> orders = new ArrayList<Order>();

    public Address() {
    }

    public Address(String streetAddress, String cityAddress, String countryAddress, Integer zipCodeAddress, Double latitudeAddress, Double longitudeAddress) {
        this.streetAddress = streetAddress;
        this.cityAddress = cityAddress;
        this.countryAddress = countryAddress;
        this.zipCodeAddress = zipCodeAddress;
        this.latitudeAddress = latitudeAddress;
        this.longitudeAddress = longitudeAddress;
    }

    public Integer getIdAddress() {
        return idAddress;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCityAddress() {
        return cityAddress;
    }

    public void setCityAddress(String cityAddress) {
        this.cityAddress = cityAddress;
    }

    public String getCountryAddress() {
        return countryAddress;
    }

    public void setCountryAddress(String countryAddress) {
        this.countryAddress = countryAddress;
    }

    public Integer getZipCodeAddress() {
        return zipCodeAddress;
    }

    public void setZipCodeAddress(Integer zipCodeAddress) {
        this.zipCodeAddress = zipCodeAddress;
    }

    public Double getLatitudeAddress() {
        return latitudeAddress;
    }

    public void setLatitudeAddress(Double latitudeAddress) {
        this.latitudeAddress = latitudeAddress;
    }

    public Double getLongitudeAddress() {
        return longitudeAddress;
    }

    public void setLongitudeAddress(Double longitudeAddress) {
        this.longitudeAddress = longitudeAddress;
    }
}