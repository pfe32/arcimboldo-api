package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class OrderedRecipe {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private OrderedRecipeId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("userId")
    @JsonIgnoreProperties("orderedRecipes")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("recipeId")
    @JsonIgnoreProperties("orderedByUsers")
    private Recipe recipe;

    private LocalDateTime date;

    public OrderedRecipe() {
    }

    public OrderedRecipe(User user, Recipe recipe, LocalDateTime date) {
        this.user = user;
        this.recipe = recipe;
        this.date = date;
        this.id = new OrderedRecipeId(user.getIdUser(), recipe.getIdRecipe());
    }
}
