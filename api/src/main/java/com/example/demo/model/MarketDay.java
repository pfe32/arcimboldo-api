package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.DayOfWeek;

@Table(name = "MarketDay")
@Entity
public class MarketDay implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "idMarketDay", nullable = false)
    private Integer idMarketDay;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("marketDays")
    private Market market;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("marketDays")
    private Producer producer;

    @Enumerated(EnumType.STRING)
    @Column(name = "dayMarketDay", nullable = false, columnDefinition = "ENUM('MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY')")
    private DayOfWeek dayMarketDay;

    public MarketDay() {
    }

    public MarketDay(Market market, Producer producer, DayOfWeek dayMarketDay) {
        this.market = market;
        this.producer = producer;
        this.dayMarketDay = dayMarketDay;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public DayOfWeek getDayMarketDay() {
        return dayMarketDay;
    }

    public void setDayMarketDay(DayOfWeek dayMarketDay) {
        this.dayMarketDay = dayMarketDay;
    }
}