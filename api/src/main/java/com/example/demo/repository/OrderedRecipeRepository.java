package com.example.demo.repository;

import com.example.demo.model.OrderedRecipe;
import com.example.demo.model.Recipe;
import com.example.demo.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderedRecipeRepository extends CrudRepository<OrderedRecipe, Integer> {

    @Query("select o.recipe from OrderedRecipe o where o.user = ?1 order by o.date desc")
    List<Recipe> findLastByUser(User user);

}