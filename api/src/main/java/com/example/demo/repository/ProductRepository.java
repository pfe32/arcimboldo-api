package com.example.demo.repository;

import com.example.demo.model.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Integer> {
    Product findByNameProduct(String name);
    Product findByIdProduct(int id);
    List<Product> findByNameProductContaining(String name);

}