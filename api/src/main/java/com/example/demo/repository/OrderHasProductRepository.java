package com.example.demo.repository;

import com.example.demo.model.OrderHasProduct;
import org.springframework.data.repository.CrudRepository;

public interface OrderHasProductRepository extends CrudRepository<OrderHasProduct, Integer> {

}