package com.example.demo.repository;

import com.example.demo.model.ParentCategorie;
import org.springframework.data.repository.CrudRepository;

public interface ParentCategorieRepository extends CrudRepository<ParentCategorie, Integer> {

}