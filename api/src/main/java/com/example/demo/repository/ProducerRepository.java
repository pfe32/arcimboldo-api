package com.example.demo.repository;

import com.example.demo.model.Producer;
import org.springframework.data.repository.CrudRepository;

public interface ProducerRepository extends CrudRepository<Producer, Integer> {

}