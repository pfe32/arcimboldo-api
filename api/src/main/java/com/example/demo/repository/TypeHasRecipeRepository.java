package com.example.demo.repository;

import com.example.demo.model.TypeHasRecipe;
import org.springframework.data.repository.CrudRepository;

public interface TypeHasRecipeRepository extends CrudRepository<TypeHasRecipe, Integer> {

}