package com.example.demo.repository;

import com.example.demo.model.Recipe;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RecipeRepository extends CrudRepository<Recipe, Integer> {

    @Query("select r from Recipe r where r.typeRecipe = 'RECIPE' order by size(r.orderedByUsers) desc")
    List<Recipe> findAllPublicRecipes();

    Recipe findByIdRecipe(int id);
    Recipe findByNameRecipe(String name);
}