package com.example.demo.repository;

import com.example.demo.model.Market;
import org.springframework.data.repository.CrudRepository;

public interface MarketRepository extends CrudRepository<Market, Integer>  {

}