package com.example.demo.repository;

import com.example.demo.model.MarketDay;
import org.springframework.data.repository.CrudRepository;

public interface MarketDayRepository extends CrudRepository<MarketDay, Integer> {

}