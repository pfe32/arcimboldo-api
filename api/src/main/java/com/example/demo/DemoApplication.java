package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	/**
	 * Maximum distance (kilometers) between the user and a market they can get products from.
	 */
	public static final int MAX_MARKET_DISTANCE = 50;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
