package com.example.demo;

import com.example.demo.model.Recipe;

import java.io.Serializable;
import java.time.DayOfWeek;
import java.util.Map;
import java.util.Set;

public class RecipeWithAvailability implements Serializable {
    public final Recipe recipe;
    public final Map<Integer, Set<DayOfWeek>> availability;

    public RecipeWithAvailability(Recipe recipe, Map<Integer, Set<DayOfWeek>> availability) {
        this.recipe = recipe;
        this.availability = availability;
    }
}