package com.example.demo;

import java.io.Serializable;

public class IdAndQuantity implements Serializable {
    private Integer id;
    private Float quantity;

    public IdAndQuantity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }
}
