package com.example.demo;

import com.example.demo.model.*;
import com.example.demo.repository.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.*;

@RestController
public class RestWebService {

    private SessionManager sessionManager;

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    OrderRepository orderRepository;
    @Autowired
    RecipeRepository recipeRepository;
    @Autowired
    TypeRepository typeRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    ProducerRepository producerRepository;
    @Autowired
    MarketRepository marketRepository;
    @Autowired
    MarketDayRepository marketDayRepository;
    @Autowired
    AddressRepository addressRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    OrderedRecipeRepository orderedRecipeRepository;
    @Autowired
    OrderHasProductRepository orderHasProductRepository;
    @Autowired
    TypeHasRecipeRepository typeHasRecipeRepository;

    public RestWebService(
            OrderRepository orderRepository,
            RecipeRepository recipeRepository,
            TypeRepository typeRepository,
            ProductRepository productRepository,
            ProducerRepository producerRepository,
            MarketRepository marketRepository,
            MarketDayRepository marketDayRepository,
            AddressRepository addressRepository,
            UserRepository userRepository,
            OrderedRecipeRepository orderedRecipeRepository,
            OrderHasProductRepository orderHasProductRepository,
            TypeHasRecipeRepository typeHasRecipeRepository
    ) {
        super();
        this.sessionManager = new SessionManager();

        this.orderRepository = orderRepository;
        this.recipeRepository = recipeRepository;
        this.typeRepository = typeRepository;
        this.productRepository = productRepository;
        this.producerRepository = producerRepository;
        this.marketRepository = marketRepository;
        this.marketDayRepository = marketDayRepository;
        this.addressRepository = addressRepository;
        this.userRepository = userRepository;
        this.orderedRecipeRepository = orderedRecipeRepository;
        this.orderHasProductRepository = orderHasProductRepository;
        this.typeHasRecipeRepository = typeHasRecipeRepository;

        /*
        // Creating a few orders
        Order o = new Order(52, "transporteur");
        Order o2 = new Order(54, "cargo");

        orderRepository.save(o);
        orderRepository.save(o2);

        // Creating a recipe with types
        Recipe ratatouille = new Recipe(
                "Ratatouille",
                50,
                "C'est une ratatouille",
                Recipe.TypeRecipe.RECIPE,
                null
        );
        recipeRepository.save(ratatouille);

        Type courgetteType = new Type("Courgette", null);
        Type tomateType = new Type("Tomate", null);
        typeRepository.save(courgetteType);
        typeRepository.save(tomateType);
        ratatouille.addType(courgetteType, 2f);
        ratatouille.addType(tomateType, 5f);
        recipeRepository.save(ratatouille);

        // Crating addresses
        Address address1 = new Address("Rue", "Paris", "France", 75000, 48.860647, 2.290729);
        addressRepository.save(address1);

        // Creating markets
        Market paris = new Market(address1);
        marketRepository.save(paris);

        // Creating producers
        Producer producer1 = new Producer("René", "rene@broceliande.fr", 123456789, new ArrayList());
        Producer producer2 = new Producer("Jean-Philippe", "jp@gmail.com", 987654321, new ArrayList());
        producerRepository.save(producer1);
        producerRepository.save(producer2);

        // Creating market days
        MarketDay day1 = new MarketDay(paris, producer1, DayOfWeek.SATURDAY);
        MarketDay day2 = new MarketDay(paris, producer2, DayOfWeek.SATURDAY);
        MarketDay day3 = new MarketDay(paris, producer2, DayOfWeek.WEDNESDAY);
        marketDayRepository.save(day1);
        marketDayRepository.save(day2);
        marketDayRepository.save(day3);

        // Creating products
        Product courgette = new Product("Courgette de René", "De la bonne courgette bien d'chez nous.", 100d, .9f, Product.UnitProduct.PIECE, courgetteType, producer1);
        Product tomate = new Product("Tomate Label Rouge", "Pas pour faire du Ketchup !", 78d, .24f, Product.UnitProduct.PIECE, tomateType, producer2);
        productRepository.save(courgette);
        productRepository.save(tomate);
        */
    }

    @RequestMapping(value = "/orders/{orderID}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Optional<Order> getOrderID(@PathVariable("orderID") Integer orderID) throws Exception {
        //Long orderNumber = Long.parseLong(orderID);
        return orderRepository.findById(orderID);
    }

    @RequestMapping(value = "/product/{productname}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Product getProductName(@PathVariable("productname") String productname) throws Exception {
        //Long orderNumber = Long.parseLong(orderID);
        return productRepository.findByNameProduct(productname);
    }

    @RequestMapping(value = "/productSearch/{productname}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ResponseEntity productSearch(@PathVariable("productname") String productname, @RequestParam Double latitude, @RequestParam Double longitude) {
        List<Product> products = productRepository.findByNameProductContaining(productname);
        ObjectMapper mapper = new ObjectMapper();
        //ArrayNode response = mapper.createArrayNode();
        List<Product> response = new ArrayList<>();
        for (Product p : products) {
            Map<Integer, Set<DayOfWeek>> availability = p.availability(latitude, longitude, 1f);
            if (!availability.isEmpty()) {
                //JsonNode node = mapper.createObjectNode();
                //((ObjectNode) node).putPOJO("product", p);
                //((ObjectNode) node).putPOJO("availability", availability);
                response.add(p);
            }
        }
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = "/product2/{productId}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Optional<Product> getProductName(@PathVariable("productId") int productId) throws Exception {
        Optional<Product> product = Optional.ofNullable(productRepository.findByIdProduct(productId));
        if (product.isPresent())
            return product;
        else
            return null;
    }

    @GetMapping("/orders")
    public Iterable<Order> getListOfOrders() {
        return orderRepository.findAll();
    }

    @RequestMapping("/messageTest")
    public String getMessage() {
        return "Hello from get";
    }

    @DeleteMapping("/orderdelete2/{id}")
    public void deleteOrder2(@PathVariable Integer id) {
        orderRepository.deleteById(id);
    }

    @PostMapping(value = "/order")
    public ResponseEntity addOrder(@RequestBody ObjectNode body, @RequestParam String session) {
        User user = this.sessionManager.getUser(session);
        // Error if the session does not exist
        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        int marketId;
        float totalPrice;
        Date marketDate;
        IdAndQuantity[] products;
        try {
            marketId = body.get("marketId").asInt();
            totalPrice = (float) body.get("totalPrice").asDouble();
            marketDate = Date.valueOf(body.get("marketDate").asText().substring(0, 10));
            ObjectMapper mapper = new ObjectMapper();
            products = mapper.treeToValue(body.get("products"), IdAndQuantity[].class);
        } catch (NullPointerException | JsonProcessingException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        Optional<Market> market = this.marketRepository.findById(marketId);
        // Error if the market does not exist
        if (!market.isPresent()) {
            return getErrorResponse("Market does not exist");
        }

        // Save order
        Order order = new Order(totalPrice, null, marketDate, user, market.get(), null);
        orderRepository.save(order);

        // Save products
        for (IdAndQuantity p : products) {
            Optional<Product> product = this.productRepository.findById(p.getId());
            // Error if the product does not exist
            if (!product.isPresent()) {
                return getErrorResponse("Product " + p.getId() + " does not exist");
            }
            this.orderHasProductRepository.save(new OrderHasProduct(order, product.get(), p.getQuantity()));
        }

        return ResponseEntity.ok().build();
    }

    /*
    @GetMapping("/products")
    public Iterable<Product> getListOfProducts() {
        return productRepository.findAll();
    }

    @RequestMapping(value="/products/{productID}", method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Optional<Product> getProductID(@PathVariable("productID") Long productID) throws Exception {
        return productRepository.findProductById(productID);
    }*/

    /**
     * Returns list of available recipes in the given list of recipes, with their availability information.
     *
     * @param recipes   Lists to return the availability of.
     * @param latitude  User position's latitude.
     * @param longitude User position's longitude.
     * @return Recipes available to the user, associated to their availability information.
     */
    private List<RecipeWithAvailability> addRecipeAvailabilities(List<Recipe> recipes, Double latitude, Double longitude) {
        List<RecipeWithAvailability> availableRecipes = new ArrayList<>();
        for (Recipe recipe : recipes) {
            Map<Integer, Set<DayOfWeek>> recipeAvailability = recipe.availability(latitude, longitude);
            if (!recipeAvailability.isEmpty()) {
                availableRecipes.add(new RecipeWithAvailability(recipe, recipeAvailability));
            }
        }
        return availableRecipes;
    }

    @GetMapping("/recipes")
    public ResponseEntity getRecipes(@RequestParam Double latitude, @RequestParam Double longitude) {
        List<RecipeWithAvailability> recipes = addRecipeAvailabilities(recipeRepository.findAllPublicRecipes(), latitude, longitude);
        return ResponseEntity.ok(recipes);
    }

    @RequestMapping(value = "/recipes/{recipeID}", method = RequestMethod.GET)
    public ResponseEntity getRecipe(@PathVariable Integer recipeID, @RequestParam Double latitude, @RequestParam Double longitude) {
        Optional<Recipe> recipe = recipeRepository.findById(recipeID);
        if (recipe.isPresent())
            return ResponseEntity.ok(new RecipeWithAvailability(recipe.get(), recipe.get().availability(latitude, longitude)));
        else
            return ResponseEntity.notFound().build();
    }

    @GetMapping("/lastOrderedRecipes")
    public ResponseEntity getLastOrderedRecipes(@RequestParam String session, @RequestParam Double latitude, @RequestParam Double longitude) {
        User user = this.sessionManager.getUser(session);
        // Error if the session does not exist
        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        List<RecipeWithAvailability> recipes = this.addRecipeAvailabilities(this.orderedRecipeRepository.findLastByUser(user), latitude, longitude);
        return ResponseEntity.ok(recipes);
    }

    @PostMapping("/addOrderedRecipes")
    public ResponseEntity addOrderedRecipe(@RequestBody ArrayNode body, @RequestParam String session) {
        User user = this.sessionManager.getUser(session);
        // Error if the session does not exist
        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        Integer[] recipeIds;
        try {
            recipeIds = mapper.treeToValue(body, Integer[].class);
        } catch (NullPointerException | JsonProcessingException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        for (int recipeId : recipeIds) {
            Optional<Recipe> recipe = this.recipeRepository.findById(recipeId);
            // Error if the recipe does not exist
            if (!recipe.isPresent()) {
                return getErrorResponse("Recipe " + recipeId + " does not exist");
            }
            OrderedRecipe orderedRecipe = new OrderedRecipe(user, recipe.get(), LocalDateTime.now());
            this.orderedRecipeRepository.save(orderedRecipe);
        }

        return ResponseEntity.ok().build();
    }

    @PostMapping("/newRecipe")
    public ResponseEntity newRecipe(@RequestBody ObjectNode body, @RequestParam String session) {
        User user = this.sessionManager.getUser(session);
        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        String nameRecipe, descriptionRecipe;
        Integer durationRecipe;
        Recipe typeRecipe;
        IdAndQuantity[] types;
        try {
            nameRecipe = body.get("name").asText();
            durationRecipe = body.get("duration").asInt();
            descriptionRecipe = body.get("description").asText();
            types = mapper.treeToValue(body.get("types"), IdAndQuantity[].class);
        } catch (NullPointerException | JsonProcessingException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        Recipe existingRecipe = this.recipeRepository.findByNameRecipe(nameRecipe);
        if (existingRecipe != null) {
            return getErrorResponse("Recipe name already taken");
        }

        // Save recipe
        Recipe newRecipe = new Recipe(nameRecipe, durationRecipe, descriptionRecipe, Recipe.TypeRecipe.CUSTOM, user);
        recipeRepository.save(newRecipe);

        // Save types
        for (IdAndQuantity t : types) {
            Optional<Type> type = this.typeRepository.findById(t.getId());
            // Error if the type does not exist
            if (!type.isPresent()) {
                return getErrorResponse("Type " + t.getId() + " does not exist");
            }
            this.typeHasRecipeRepository.save(new TypeHasRecipe(type.get(), newRecipe, t.getQuantity()));
        }

        ObjectMapper mapper = new ObjectMapper();
        JsonNode response = mapper.createObjectNode();
        ((ObjectNode) response).put("recipeId", newRecipe.getIdRecipe());
        return ResponseEntity.ok().body(response);
    }

    /*@RequestMapping(value = "/deleteRecipe", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Boolean deleteRecipe(@RequestParam(value = "recipeId", required = true) Integer recipeId) throws Exception {
       Boolean isRemoved = this.recipes.removeIf(r -> r.getIdRecipe().equals(recipeId));
        return isRemoved;
    }*/

    @PutMapping("/put")
    public @ResponseBody
    ResponseEntity<String> put(@RequestParam String session) {
        User user = this.sessionManager.getUser(session);
        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        return new ResponseEntity<String>("PUT Response", HttpStatus.OK);
    }

    @PutMapping("/updateRecipe/{id}")
    ResponseEntity<String> replaceRecipe(@RequestBody Recipe newRecipe, @PathVariable Integer id, @RequestParam String session) {
        User user = this.sessionManager.getUser(session);
        // Error if the session does not exist
        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        recipeRepository.findById(id)
                .map(recipe -> {
                    recipe.setNameRecipe(newRecipe.getNameRecipe());
                    recipe.setDescriptionRecipe(newRecipe.getDescriptionRecipe());
                    recipe.setDurationRecipe(newRecipe.getDurationRecipe());
                    recipe.setTypes(newRecipe.getTypes());

                    return recipeRepository.save(recipe);
                })
                .orElseGet(() -> {
                    newRecipe.setIdRecipe(id);
                    return recipeRepository.save(newRecipe);
                });
        return new ResponseEntity<String>(String.valueOf(newRecipe.getIdRecipe()), HttpStatus.OK);
    }

    @DeleteMapping("/deleteRecipe2/{id}")
    public ResponseEntity<Object> deleteRecipe2(@PathVariable Integer id, @RequestParam String session) {
        User user = this.sessionManager.getUser(session);
        // Error if the session does not exist
        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        try {
            recipeRepository.deleteById(id);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return new ResponseEntity<>("Recipe deleted ", HttpStatus.OK);
    }

    @PostMapping(value = "/availableProducts")
    public ResponseEntity getRecipe(@RequestBody ArrayNode body, @RequestParam Double latitude, @RequestParam Double longitude) {
        ObjectMapper mapper = new ObjectMapper();
        IdAndQuantity[] products;
        try {
            products = mapper.treeToValue(body, IdAndQuantity[].class);
        } catch (NullPointerException | JsonProcessingException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        //ArrayNode response = mapper.createArrayNode();
        List<Product> response = new ArrayList<>();
        for (IdAndQuantity p : products) {
            Optional<Product> product = this.productRepository.findById(p.getId());
            // Error if the product does not exist
            if (!product.isPresent()) {
                return getErrorResponse("Product " + p.getId() + " does not exist");
            }
            Map<Integer, Set<DayOfWeek>> availability = product.get().availability(latitude, longitude, 1f);
            if (!availability.isEmpty()) {
                //JsonNode node = mapper.createObjectNode();
                //((ObjectNode) node).putPOJO("product", product.get());
                //((ObjectNode) node).putPOJO("availability", availability);
                response.add(product.get());
            } else {
                // Find the cheapest product of the same type that is available
                Product cheapest = null;
                for (Product otherProduct : product.get().getType().getProducts()) {
                    if (otherProduct.getIdProduct() != product.get().getIdProduct() && !otherProduct.availability(latitude, longitude, 1f).isEmpty()) {
                        if (cheapest == null || cheapest.getPriceProduct() > otherProduct.getPriceProduct()) {
                            cheapest = otherProduct;
                        }
                    }
                }
                if (cheapest != null) {
                    response.add(cheapest);
                }
            }
        }
        return ResponseEntity.ok(response);
    }

    @PostMapping("/newAccount")
    public ResponseEntity newAccount(@RequestBody ObjectNode body) {
        String email, username, password;
        try {
            email = body.get("email").asText();
            username = body.get("username").asText();
            password = body.get("password").asText();
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        User existingUser = this.userRepository.findByEmailUser(email);

        // Error if the user already exists
        if (existingUser != null) {
            return getErrorResponse("Email already taken");
        }

        // Create the user
        User newUser = new User(email, username, BCrypt.hashpw(password, BCrypt.gensalt()));
        userRepository.save(newUser);

        // Return session token
        ObjectMapper mapper = new ObjectMapper();
        JsonNode response = mapper.createObjectNode();
        ((ObjectNode) response).put("session", this.sessionManager.newSession(newUser));
        ((ObjectNode) response).set("user", mapper.valueToTree(newUser));
        return ResponseEntity.ok(response);
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody ObjectNode body) {
        String email, password;
        try {
            email = body.get("email").asText();
            password = body.get("password").asText();
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        User user = this.userRepository.findByEmailUser(email);

        // Error if the user does not exist
        if (user == null) {
            return getErrorResponse("Invalid credentials");
        }
        // Error if the password is incorrect
        else if (!BCrypt.checkpw(password, user.getPasswordUser())) {
            return getErrorResponse("Invalid credentials");
        } else {
            // Success
            // Return session token
            ObjectMapper mapper = new ObjectMapper();
            JsonNode response = mapper.createObjectNode();
            ((ObjectNode) response).put("session", this.sessionManager.newSession(user));
            ((ObjectNode) response).set("user", mapper.valueToTree(user));
            return ResponseEntity.ok(response);
        }
    }

    @GetMapping("/checkSession")
    public ResponseEntity checkSession(@RequestParam String session) {
        User user = this.sessionManager.getUser(session);
        // Error if the session does not exist
        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        } else {
            return ResponseEntity.ok(user);
        }
    }

    /**
     * Creates a ResponseEntity containing an error to send back from the API.
     *
     * @param errorMessage Text of the error to send.
     * @return Response containing the error in Json.
     */
    private ResponseEntity getErrorResponse(String errorMessage) {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode response = mapper.createObjectNode();
        ((ObjectNode) response).put("error", errorMessage);
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(response);
    }
}


