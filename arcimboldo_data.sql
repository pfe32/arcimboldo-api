insert into user values (1, "1996-05-05", "pierre.de.faget.de.casteljau@efrei.net", "$2a$10$D4uKbl0T/5mif1OxU7L.KO32Pu8ab9Nt/FxPT5wMVHfr4uE45PsIy" , "Pierre de Faget de Casteljau");    
insert into user values (2, "1997-01-01", "anthony.morali@efrei.net", "$2a$10$D4uKbl0T/5mif1OxU7L.KO32Pu8ab9Nt/FxPT5wMVHfr4uE45PsIy" , "Anthony Morali");    
insert into user values (3, "1997-01-02", "tristan.le.bras@efrei.net", "$2a$10$D4uKbl0T/5mif1OxU7L.KO32Pu8ab9Nt/FxPT5wMVHfr4uE45PsIy" , "Tristan LE BRAS");    
insert into user values (4, "1997-01-03", "gaetan.nobre@efrei.net", "$2a$10$D4uKbl0T/5mif1OxU7L.KO32Pu8ab9Nt/FxPT5wMVHfr4uE45PsIy" , "Gaëtan NOBRE");    
insert into user values (5, "1997-01-04", "vincent.roche@efrei.net", "$2a$10$D4uKbl0T/5mif1OxU7L.KO32Pu8ab9Nt/FxPT5wMVHfr4uE45PsIy" , "Vincent ROCHE");    
insert into user values (6, "1997-01-05", "amaury.paquis-thonat@efrei.net", "$2a$10$D4uKbl0T/5mif1OxU7L.KO32Pu8ab9Nt/FxPT5wMVHfr4uE45PsIy" , "Amaury PAQUIS-THONAT");    

insert into	address values(	1	,	"Versailles"	,	"France", 48.803178, 2.126502, 1,	78000);
insert into	address values(	2	,	"Versailles"	,	"France", 48.803178, 2.126502, 2,	78000);
insert into	address values(	3	,	"Versailles"	,	"France", 48.803178, 2.126502, 3,	78000);
insert into	address values(	4	,	"Le Chesnay"	,	"France", 48.82247, 2.127319, 4,	78150);
insert into	address values(	5	,	"Trappes"	,	"France", 48.776726, 1.986634, 5,	78190);
insert into	address values(	6	,	"Trappes"	,	"France", 48.776726, 1.986634, 6,	78190);
insert into	address values(	7	,	"Marseille"	,	"France", 43.295417, 5.374007, 7,	13000);

insert into	market	values(1, 1);										
insert into	market	values(2, 1);										
insert into	market	values(3, 1);										
insert into	market	values(4, 2);										
insert into	market	values(5, 2);										
insert into	market	values(6, 3);
insert into	market	values(7, 7);

insert into	orders	values(	1, "", "2020-03-01", 0 , 6, 1, 1);								
insert into	orders	values(	2, "", "2020-03-01", 0 , 6, 2, 2);								
insert into	orders	values(	3, "", "2020-03-01", 0 , 6, 3, 3);								
insert into	orders	values(	4, "", "2020-03-01", 0 , 6, 1, 3);								
insert into	orders	values(	5, "", "2020-03-01", 0 , 6, 2, 2);								
insert into	orders	values(	6, "", "2020-03-01", 0 , 6, 3, 1);

insert into	producer	values(1, "Dupond@gmail.com", "Dupond",	0033101010101);						
insert into	producer	values(2, "Durand@gmail.com", "Durand",	0033102020202);						
insert into	producer	values(3, "Dupuis@gmail.com", "Dupuis",	0033103030303);						
insert into	producer	values(4, "Martin@gmail.com", "Martin",	0033104040404);						
insert into	producer	values(5, "Tartempion@gmail.com", "Tartempion",	0033105050505);						
insert into	producer	values(6, "Bichoux@gmail.com", "Bichoux",	0033106060606);						


INSERT INTO type (id_type, name_type) VALUES
(	1	,	"Abricot"),
(	2	,	"Ail"),
(	3	,	"Amande"),
(	4	,	"Artichaut"),
(	5	,	"Aubergine"),
(	6	,	"Betterave rouge"),
(	7	,	"Brugnon"),
(	8	,	"Carotte"),
(	9	,	"Cassis"),
(	10,	"Céleri"),
(	11,	"Cerise"),
(	12,	"Châtaigne"),
(	13,	"Citron niçois"),
(	14,	"Clémentine"),
(	15,	"Coing"),
(	16,	"Courgette"),
(	17,	"Datte"),
(	18,	"Endive"),
(	19,	"Épinard"),
(	20,	"Feijoa"),
(	21,	"Fève verte"),
(	22,	"Fraise"),
(	23,	"Framboise"),
(	24,	"Groseille"),
(	25,	"Haricot sec"),
(	26,	"Haricot vert"),
(	27,	"Kaki"),
(	28,	"Kiwi"),
(	29,	"Laitue"),
(	30,	"Laurier"),
(	31,	"Mandarine"),
(	32,	"Melon"),
(	33,	"Mirabelle"),
(	34,	"Mûre"),
(	35,	"Myrtille"),
(	36,	"Navet"),
(	37,	"Nectarine"),
(	38,	"Noix"),
(	39,	"Oignon"),
(	40,	"Orange"),
(	41,	"Pastèque"),
(	42,	"Pêche"),
(	43,	"Persil"),
(	44,	"Poireau"),
(	45,	"Poire"),
(	46,	"Poivron rouge"),
(	47,	"Poivron vert"),
(	48,	"Pomme Golden"),
(	49,	"Pomme Gala"),
(	50,	"Pomme Granny Smith"),
(	51,	"Pomme Reinette grise"),
(	52,	"Pomme Boskoop"),
(	53,	"Pomme Antarès"),
(	54,	"Pomme Braeburn"),
(	55,	"Pomme Jonagold"),
(	56,	"Pomme Elstar"),
(	57,	"Pomme Ariane"),
(	58,	"Pomme Jazz"),
(	59,	"Pomme Tentation®"),
(	60,	"Pomme Pink Lady®"),
(	61,	"Pomme Fuji"),
(	62,	"Pomme Idared"),
(	63,	"Pomme Delicious rouge"),
(	64,	"Pomme de terre"),
(	65,	"Prune"),
(	66,	"Raisin"),
(	67,	"Soja sec"),
(	68,	"Thym"),
(	69,	"Tomate");

ALTER TABLE `product` CHANGE `description_product` `description_product` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

insert into	product (id_product, type_id_type, name_product, description_product, amount_product, price_product, unit_product, producer_id_producer, image_url_product)
values
(	1	,	1	,	"Abricot"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://fr.rc-cdn.community.thermomix.com/recipeimage/j5t0blb3-4581e-791258-cfcd2-gzxqn78b/766b0d43-8638-48ad-babd-4965704b675d/main/flan-a-labricot.jpg"),
(	2	,	2	,	"Ail"	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://img-3.journaldesfemmes.fr/Hi8QypZxkq2-fKCiH3-psDvtJgU=/910x607/smart/0a35cb5fd4db437e97449e2d13204e07/ccmcms-jdf/10663490.jpg"),
(	3	,	3	,	"Amande"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://img-3.journaldesfemmes.fr/FT_yGfFOkmdSOvdP2NGombAmx3w=/910x607/smart/bcbe6b90aa1b42d780b6d2793e98ada4/ccmcms-jdf/11158029.jpg"),
(	4	,	4	,	"Artichaut"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://www.havea.com/media/catalog/product/1/d/1d25c90feb993ef38b5df330c6c0700830883610_artichaut_min.jpg"),
(	5	,	5	,	"Aubergine"	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://www.macuisinesante.com/wp-content/uploads/2014/08/aubergine2.jpg"),
(	6	,	6	,	"Betterave rouge"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://static.potageons.fr/250-large_default/graines-de-betterave-rouge-plate-egypte.jpg"),
(	7	,	7	,	"Brugnon"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://static.passeportsante.net/200x200/i41580-le-brugnon.jpg"),
(	8	,	8	,	"Carotte"	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://img-3.journaldesfemmes.fr/4PYoBAo1J5rsDbuMiSTNRGolvhc=/910x607/smart/3b81125b1cbd46f7af87766bb8430152/ccmcms-jdf/10659145.jpg"),
(	9	,	9	,	"Cassis"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://media.gerbeaud.net/2011/07/cassis.jpg"),
(	10,	10,	"Céleri" 	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://sante.lefigaro.fr/sites/default/files/img/2012/02/28/menual093_0.jpg"),
(	11,	11,	"Cerise"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://observatoire-des-aliments.fr/wp-content/uploads/2014/06/cerise2.jpg"),
(	12,	12,	"Châtaigne"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://www.cuisinealafrancaise.com/img/thumbs/Chataignes-92671aacb33562a3d506c73370f26ace.jpg"),
(	13,	13,	"Citron niçois"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://jardinsdevartan.com/wp-content/uploads/2016/11/citron-jaune-jardins-de-vartan.jpg"),
(	14,	14,	"Clémentine"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://www.academiedugout.fr/images/8551/370-274/ffffff/fotolia_59571177_subscription_xxl.jpg?poix=50&poiy=50"),
(	15,	15,	"Coing"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://img-3.journaldesfemmes.fr/eHCrkYrZYAfN8c4hZ7wq3WvUG7o=/910x607/smart/9163fdaa2b6243d48fa41b2bab7bf505/ccmcms-jdf/10663047.jpg"),
(	16,	16,	"Courgette"	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://img-3.journaldesfemmes.fr/PVIbH_pGxMFhGPl8oEkWr3DbtaY=/910x607/smart/3d4af6fac7234c2283fd7d9cf34e6981/ccmcms-jdf/10659281.jpg"),
(	17,	17,	"Datte"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://yupik.com/media/catalog/product/cache/c687aa7517cf01e65c009f6943c2b1e9/3/0/30010p_1.jpg"),
(	18,	18,	"Endive"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://espritfraicheur.fr/225-large_default/endive-.jpg"),
(	19,	19,	"Épinard"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://img-3.journaldesfemmes.fr/BXxRIUk8ho6av0o1tQsIL8vkisE=/910x607/smart/a5b064650d0843e2bff9586d33eee2ee/ccmcms-jdf/10659324.jpg"),
(	20,	20,	"Feijoa"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://www.seeds-gallery.shop/901-large_default/graines-de-le-feijoa.jpg"),
(	21,	21,	"Fève verte"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://img-3.journaldesfemmes.fr/__oIluUBnuvIuFk9bH7DeGmT1BE=/910x607/smart/2b9c1e2dca5f46278d098c546a3d39b5/ccmcms-jdf/10661476.jpg"),
(	22,	22,	"Fraise"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://img-3.journaldesfemmes.fr/LFYLLU8JV163I7rcv9-y_SrLuTs=/910x607/smart/e2d84215f5c44c648c3ecdcbf0ac1ccc/ccmcms-jdf/10662418.jpg"),
(	23,	23,	"Framboise"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://img-3.journaldesfemmes.fr/lZqm2rIC2PMKPP1zVrNP7uFsyoo=/910x607/smart/91eed6d1ec5c4912849bc192c62161ab/ccmcms-jdf/10662426.jpg"),
(	24,	24,	"Groseille"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://img-3.journaldesfemmes.fr/HvUxalrx_xbQ5ezqFjQ3UxFsf70=/910x607/smart/153bc96c64024825b150f5ebb8921b7c/ccmcms-jdf/10662428.jpg"),
(	25,	25,	"Haricot sec"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://img-3.journaldesfemmes.fr/xihMVP6nr4CSUtctPCwZNqj6Mks=/910x607/smart/201440dea12e48a2add4b36362c2404f/ccmcms-jdf/10663996.jpg"),
(	26,	26,	"Haricot vert"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://img-3.journaldesfemmes.fr/EB559LzErF0e-mjSNjI2NMO1ySI=/910x607/smart/ff668b99ffd94d5997a34e5bc4518fe7/ccmcms-jdf/10659812.jpg"),
(	27,	27,	"Kaki"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://img-3.journaldesfemmes.fr/9STl3jjP20eWvcC_SHl5rMzovYk=/910x607/smart/76c797992a9a48efb5994cf25328eb42/ccmcms-jdf/10663221.jpg"),
(	28,	28,	"Kiwi"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Kiwi_%28Actinidia_chinensis%29_1_Luc_Viatour.jpg/1200px-Kiwi_%28Actinidia_chinensis%29_1_Luc_Viatour.jpg"),
(	29,	29,	"Laitue"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://img-3.journaldesfemmes.fr/Hwbeol52Jz1DRBwgjBjrkmDF4Zg=/910x607/smart/e768c994317848edb31ced2ab6a2f35e/ccmcms-jdf/10661612.jpg"),
(	30,	30,	"Laurier"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://img-3.journaldesfemmes.fr/su2CpR44y1N0XFKbhMDu4Lyrk1U=/910x607/smart/7017fb6aa9e543ab932744a788342e64/ccmcms-jdf/10662950.jpg"),
(	31,	31,	"Mandarine"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://img-3.journaldesfemmes.fr/Kzj8RKSWi6rqF9RJ3exWH8UB-hE=/910x607/smart/2999942f22784358bbcecbd204d89fb3/ccmcms-jdf/10660242.jpg"),
(	32,	32,	"Melon"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://www.carrefour.fr/media/1500x1500/Photosite/PRODUITS_FRAIS_TRANSFORMATION/FRUITS_ET_LEGUMES/3276554651662_PHOTOSITE_20160322_164303_0.jpg?placeholder=1"),
(	33,	33,	"Mirabelle"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://photos.gammvert.fr/v5/products/full/48892-prunier-mirabelle-de-nancy-taille-en-palmette-oblique-en-pot-7.jpeg"),
(	34,	34,	"Mûre"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://img-3.journaldesfemmes.fr/vN3v3_yngfUGoom0HMbwcjr00IU=/910x607/smart/383c69cd158e4ecd9899d9dfebc68c21/ccmcms-jdf/10660803.jpg"),
(	35,	35,	"Myrtille"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://img-3.journaldesfemmes.fr/BaLyZkPOkZH_dOIpzcwzMocwhRA=/910x607/smart/d09a606f46454f1b901cd05d7e403894/ccmcms-jdf/10660230.jpg"),
(	36,	36,	"Navet"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://img-3.journaldesfemmes.fr/y-auOCxDIyEkR89uSTZLGONOKBg=/910x607/smart/ce03be2649df493fa744b7e0f34d73ac/ccmcms-jdf/10659858.jpg"),
(	37,	37,	"Nectarine"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cb/White_nectarine_and_cross_section02_edit.jpg/1200px-White_nectarine_and_cross_section02_edit.jpg"),
(	38,	38,	"Noix"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://www.pourquoidocteur.fr/media/article/thunbs/uploded_istock-599999924-1564484275.jpg"),
(	39,	39,	"Oignon"	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://www.academiedugout.fr/images/15721/370-274/ffffff/fotolia_55631648_subscription_xl-copy.jpg?poix=50&poiy=50"),
(	40,	40,	"Orange"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://1001perles.com/wp-content/uploads/2016/06/orange-web-1024x768.jpg"),
(	41,	41,	"Pastèque"	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://img-3.journaldesfemmes.fr/1-iMn1fZcrNWgOpfU9XNoAm-sKo=/910x607/smart/6fcf77e6558b43f69c40c8ab4e822ccd/ccmcms-jdf/10662003.jpg"),
(	42,	42,	"Pêche"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://img-3.journaldesfemmes.fr/IrV3p2mnAxSul6Oj1jjsk1Xofxk=/910x607/smart/23f0e9d66c2745c8b36b1a4c3c79d1a2/ccmcms-jdf/10662450.jpg"),
(	43,	43,	"Persil"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://www.veritable-potager.fr/185-thickbox_default/lingot-persil-plat.jpg"),
(	44,	44,	"Poireau"	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://cache.marieclaire.fr/data/photo/w1000_c17/cuisine/4m/photo-de-poireau.jpg"),
(	45,	45,	"Poire"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://img-3.journaldesfemmes.fr/l1ow5kF658mOy2kncJTuzYDUbCA=/910x607/smart/80eb226fcf034f27b481eca5dfe71f8f/ccmcms-jdf/10662618.jpg"),
(	46,	46,	"Poivron rouge"	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://az836796.vo.msecnd.net/media/image/product/fr/medium/0000000094088.jpg"),
(	47,	47,	"Poivron vert"	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://www.academiedugout.fr/images/8383/370-274/ffffff/fotolia_52283514_subscription_xl-copy.jpg?poix=50&poiy=50"),
(	48,	48,	"Pomme Golden"	,	"La Golden est la variété la plus consommée en France. C’est aussi la seule qui bénéficie de signes officiels de qualité et d’origine.
Robe : La Golden est de couleur jaune avec un soupçon de vert. Elle présente parfois une légère coloration rouge sur la face exposée au soleil, surtout lorsqu’elle est cultivée en altitude.
Production : 35 % des pommes produites en France sont des Golden. Résistante à la sécheresse et s’adaptant à tous les types de porte-greffe , les Golden sont réputées pour leur culture facile.
En bouche : Croquante, sucrée, douce. La Golden produite en altitude est réputée avoir davantage de goût.
Préparations préférées : A croquer, au four, en tarte, à cuisiner"	,	50	,	3	,	'PIECE', 1, "https://produits.bienmanger.com/35147-0w470h470_Pommes_Golden_Aop_Limousin_Bio.jpg"),
(	49,	49, "Pomme Gala"	,	"La Gala occupe la deuxième place dans le cœur des Français. Sans doute pour son éclat appétissant et la douceur de sa chair !
Robe : Bicolore, la Gala affiche une dominante d’un rouge intense.
Production : 16  % des pommes produites en France sont des Gala et dérivés : Galaxy, Annaglo, Brookfield ® Baigent, Buckeye ® Simmons, Cherry Gala, Burkitt ® Gala.
En bouche : Très croquante, sucrée, juteuse. Idéale pour vous désaltérer.
Préparations préférées : A croquer, au four, en tarte, à cuisiner."	,	50	,	3	,	'PIECE', 1, "https://fridg-front.s3.amazonaws.com/media/products/332.jpg"),
(	50,	50, "Pomme Granny Smith"	,	"Sa saveur aigre-douce et sa chair ferme et croquante en font la troisième pomme préférée des Français. La Granny Smith, une pomme tonique !
Robe : La Granny Smith présente une peau épaisse d’une jolie couleur vert vif.
Production : 12 % des pommes produites en France sont des Granny Smith, essentiellement produites dans le Sud-Est de la France.
En bouche : Très croquante, acidulée, juteuse. Elle fait pétiller les papilles !
Préparations préférées : A croquer, en salade"	,	50	,	3	,	'PIECE', 1, "https://fridg-front.s3.amazonaws.com/media/products/1227.jpg"),
(	51,	51, "Pomme Reinette grise du Canada"	,	"Malgré son drôle de nom, la Reinette grise du Canada est une ancienne variété française. Parfaite à la cuisson, elle se croque aussi très volontiers.
Robe : La Reinette grise du Canada est de forme légèrement aplatie. Sa peau est d’une texture rugueuse et d’une couleur qui évoque l’automne : brun à vert bronze pouvant tirer sur le roux.
Production : Elle constitue 3 % des pommes produites en France tant dans le Val de Loire et le Sud-Est que dans le Sud-Ouest.
En bouche : Croquante, acidulée, sa saveur est relevée et ses arômes complexes. Son bouquet va vous charmer.
Préparations préférées : A croquer, au four."	,	50	,	3	,	'PIECE', 1, "https://allopaniers.fr/wp-content/uploads/2019/10/pomme-canada.jpg"),
(	52,	52,	"Pomme Boskoop"	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://biofermier.com/mulhouse/wp-content/uploads/sites/14/2016/10/Belle-de-Boskoop.jpg"),
(	53,	53,	"Pomme Antarès"	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://www.grandfrais.com/userfiles/image/produit/grand-frais-pomme-antares.png"),
(	54,	54,	"Pomme Braeburn"	,	"Originaire de Nouvelle-Zélande, la Braeburn est une variété récente, produite et consommée sur toute la surface du globe.
Robe : Son apparence dissymétrique et sa couleur rouge brique légèrement striée rendent la Braeburn reconnaissable en un clin d’œil.
Production : Elle correspond à plus de 6 % de la production française.
En bouche : Très croquante, acide et sucrée, juteuse. Un vrai festival de sensations !
Préparations préférées : A croquer, en tarte, au four."	,	50	,	3	,	'PIECE', 1, "https://www.ledomainedes3b.fr/wp-content/uploads/2016/11/pomme-breaburn.png"),
(	55,	55,	"Pomme Jonagold"	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://luchyprimeurs.com/66-large_default/jonagold.jpg"),
(	56,	56,	"Pomme Elstar"	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://allopaniers.fr/wp-content/uploads/2019/12/elstar-2.jpg"),
(	57,	57,	"Pomme Ariane" 	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://www.ledomainedes3b.fr/wp-content/uploads/2018/01/Pomme-ariane.png"),
(	58,	58,	"Pomme Jazz"	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://www.aligro.ch/media/cache/article_wide/article_images/50e/50e86424c1e55edd2d4c86d0b9e05e477ac42f2b.jpeg"),
(	59,	59,	"Pomme Tentation®" 	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://fridg-front.s3.amazonaws.com/media/products/pomme_tentation.jpg"),
(	60,	60,	"Pomme Pink Lady®"	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://static.750g.com/images/622-auto/52f8ab6d9661295e1111d280b4ac1e38/une-pomme-vue-de-face-avec-gouttes.jpg"),
(	61,	61,	"Pomme Fuji"	,	"Originaire du Japon, la Fuji est une pomme très rafraichissante. On la préfère crue, croquée au naturel.
Robe : Fruit assez gros et de forme bien ronde, la Fuji présente une peau couleur vert délavé sur fond rose.
Production : La Fuji constitue 3,7 % de la production française.
En bouche : Croquante, sucrée, très juteuse. Parfaite pour un jus de pommes maison !
Préparations préférées : A croquer."	,	50	,	3	,	'PIECE', 1, "https://az836796.vo.msecnd.net/media/image/product/fr/medium/0000000094129.jpg"),
(	62,	62,	"Pomme Idared"	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://media.istockphoto.com/photos/idared-apple-isolated-on-white-picture-id660222508?k=6&m=660222508&s=170667a&w=0&h=ZXOBYRbz4-mh3MzexZn7UrWKiGvBJSjKmgQ3AWTycXU="),
(	63,	63,	"Pomme Delicious rouge"	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://media.houra.fr/ART-IMG-XL/18/01/2050000320118-3.jpg"),
(	64,	64,	"Pomme de terre"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://img-3.journaldesfemmes.fr/67Rsw6hNSY75ECliUWuNb8WXkY0=/910x607/smart/ca0c3d6738544216bc1ea27163367eae/ccmcms-jdf/10660791.jpg"),
(	65,	65,	"Prune"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://sante.lefigaro.fr/sites/default/files/img/2012/02/28/menual078_0.jpg"),
(	66,	66,	"Raisin"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://fac.img.pmdstatic.net/fit/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2FFAC.2Fcontent.2Fuploads.2F2018.2F09.2Fraisin-1.2Ejpg/748x372/quality/90/crop-from/center/les-5-bienfaits-sante-du-raisin.jpeg"),
(	67,	67,	"Soja sec"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://www.celnat.fr/wp-content/uploads/2017/08/graines-soja-jaune.png"),
(	68,	68,	"Thym"	,	"Description..."	,	50	,	3	,	'KG', 1, "https://cdn.radiofrance.fr/s3/cruiser-production/2019/09/4d626b6e-59e1-44bc-b16e-8750395fe1ac/870x489_gettyimages-1140944080.jpg"),
(	69,	69,	"Tomate"	,	"Description..."	,	50	,	3	,	'PIECE', 1, "https://lesprosdelapetiteenfance.fr/sites/default/files/tomate_1.jpg"),
(	70,	48,	"Pomme Golden"	,	"Les bonnes pommes des Calanques"	,	20	,	1.2 / (1000 / 150)	,	'PIECE', 6, "https://produits.bienmanger.com/35147-0w470h470_Pommes_Golden_Aop_Limousin_Bio.jpg"),
(	71,	49, "Pomme Gala"	,	"Les bonnes pommes des Calanques"	,	50	,	2.1 / (1000 / 150)	,	'PIECE', 6, "https://fridg-front.s3.amazonaws.com/media/products/332.jpg"),
(	72,	50, "Pomme Granny Smith"	,	"Les bonnes pommes des Calanques"	,	50	,	1.2 / (1000 / 150)	,	'PIECE', 6, "https://fridg-front.s3.amazonaws.com/media/products/1227.jpg"),
(	73,	51, "Pomme Reinette grise du Canada"	,	"Les bonnes pommes des Calanques"	,	50	,	1.2 / (1000 / 150)	,	'PIECE', 6, "https://allopaniers.fr/wp-content/uploads/2019/10/pomme-canada.jpg"),
(	74,	52,	"Pomme Boskoop"	,	"Les bonnes pommes des Calanques"	,	50	,	1.2 / (1000 / 150)	,	'PIECE', 6, "https://biofermier.com/mulhouse/wp-content/uploads/sites/14/2016/10/Belle-de-Boskoop.jpg"),
(	75,	53,	"Pomme Antarès"	,	"Les bonnes pommes des Calanques"	,	50	,	1.2 / (1000 / 150)	,	'PIECE', 6, "https://www.grandfrais.com/userfiles/image/produit/grand-frais-pomme-antares.png"),
(	76,	54,	"Pomme Braeburn"	,	"Les bonnes pommes des Calanques"	,	50	,	1.2 / (1000 / 150)	,	'PIECE', 6, "https://www.ledomainedes3b.fr/wp-content/uploads/2016/11/pomme-breaburn.png"),
(	77,	55,	"Pomme Jonagold"	,	"Les bonnes pommes des Calanques"	,	50	,	1.2 / (1000 / 150)	,	'PIECE', 6, "https://luchyprimeurs.com/66-large_default/jonagold.jpg"),
(	78,	56,	"Pomme Elstar"	,	"Les bonnes pommes des Calanques"	,	50	,	1.2 / (1000 / 150)	,	'PIECE', 6, "https://allopaniers.fr/wp-content/uploads/2019/12/elstar-2.jpg"),
(	79,	57,	"Pomme Ariane" 	,	"Les bonnes pommes des Calanques"	,	50	,	1.22 / (1000 / 150)	,	'PIECE', 6, "https://www.ledomainedes3b.fr/wp-content/uploads/2018/01/Pomme-ariane.png"),
(	80,	58,	"Pomme Jazz"	,	"Les bonnes pommes des Calanques"	,	50	,	1.2 / (1000 / 150)	,	'PIECE', 6, "https://www.aligro.ch/media/cache/article_wide/article_images/50e/50e86424c1e55edd2d4c86d0b9e05e477ac42f2b.jpeg"),
(	81,	59,	"Pomme Tentation®" 	,	"Les bonnes pommes des Calanques"	,	50	,	3 / (1000 / 150)	,	'PIECE', 6, "https://fridg-front.s3.amazonaws.com/media/products/pomme_tentation.jpg"),
(	82,	60,	"Pomme Pink Lady®"	,	"Les bonnes pommes des Calanques"	,	50	,	2.9 / (1000 / 150)	,	'PIECE', 6, "https://static.750g.com/images/622-auto/52f8ab6d9661295e1111d280b4ac1e38/une-pomme-vue-de-face-avec-gouttes.jpg"),
(	83,	61,	"Pomme Fuji"	,	"Les bonnes pommes des Calanques"	,	50	,	2 / (1000 / 150)	,	'PIECE', 6, "https://az836796.vo.msecnd.net/media/image/product/fr/medium/0000000094129.jpg"),
(	84,	62,	"Pomme Idared"	,	"Les bonnes pommes des Calanques"	,	50	,	1.2 / (1000 / 150)	,	'PIECE', 6, "https://media.istockphoto.com/photos/idared-apple-isolated-on-white-picture-id660222508?k=6&m=660222508&s=170667a&w=0&h=ZXOBYRbz4-mh3MzexZn7UrWKiGvBJSjKmgQ3AWTycXU="),
(	85,	63,	"Pomme Delicious rouge"	,	"Les bonnes pommes des Calanques"	,	50	,	1.17 / (1000 / 150)	,	'PIECE', 6, "https://media.houra.fr/ART-IMG-XL/18/01/2050000320118-3.jpg");


INSERT INTO product (id_product, unit_product, price_product) VALUES
(	1	,	'KG'	,	6.9 ),
(	2	,	'PIECE'	,	2.5 / (1000 / 15)	),
(	3	,	'KG'	,	6.24	),
(	4	,	'KG'	,	1.46	),
(	5	,	'PIECE'	,	2.1 / (1000 / 225)	),
(	6	,	'KG'	,	1.4	),
(	7	,	'KG'	,	0.65	),
(	8	,	'PIECE'	,	1.3 / (1000 / 125)	),
(	9	,	'KG'	,	19.2	),
(	10	,	'PIECE'	,	1.25 / (1000 / 30)	),
(	11	,	'KG'	,	4.34	),
(	12	,	'KG'	,	3.5	),
(	13	,	'KG'	,	1.5	),
(	14	,	'KG'	,	3.24	),
(	15	,	'KG'	,	3.36	),
(	16	,	'PIECE'	,	2.5 / (1000 / 300)	),
(	17	,	'KG'	,	15.5	),
(	18	,	'KG'	,	2.5	),
(	19	,	'KG'	,	5.2	),
(	20	,	'KG'	,	6.5	),
(	21	,	'KG'	,	2.33	),
(	22	,	'KG'	,	4.32	),
(	23	,	'KG'	,	6.47	),
(	24	,	'KG'	,	5.74	),
(	25	,	'KG'	,	5.7	),
(	26	,	'KG'	,	2.5	),
(	27	,	'KG'	,	2.5	),
(	28	,	'KG'	,	3.25	),
(	29	,	'KG'	,	2.54	),
(	30	,	'KG'	,	2.5	),
(	31	,	'KG'	,	5.47	),
(	32	,	'PIECE'	,	1.5	),
(	33	,	'KG'	,	3	),
(	34	,	'KG'	,	18.5	),
(	35	,	'KG'	,	16.6	),
(	36	,	'KG'	,	1.36	),
(	37	,	'KG'	,	2.5	),
(	38	,	'KG'	,	2.7	),
(	39	,	'PIECE'	,	0.35	),
(	40	,	'KG'	,	3.7	),
(	41	,	'PIECE'	,	0.5	),
(	42	,	'KG'	,	3.45	),
(	43	,	'KG'	,	1.5	),
(	44	,	'PIECE'	,	2.8 / (1000 / 150)	),
(	45	,	'KG'	,	3.7	),
(	46	,	'PIECE'	,	1.4 / (1000 / 160)	),
(	47	,	'PIECE'	,	1.4 / (1000 / 160)	),
(	48	,	'PIECE'	,	1.24 / (1000 / 150)	),
(	49	,	'PIECE'	,	2.5 / (1000 / 150)	),
(	50	,	'PIECE'	,	1.24 / (1000 / 150)	),
(	51	,	'PIECE'	,	1.24 / (1000 / 150)	),
(	52	,	'PIECE'	,	1.24 / (1000 / 150)	),
(	53	,	'PIECE'	,	1.24 / (1000 / 150)	),
(	54	,	'PIECE'	,	1.24 / (1000 / 150)	),
(	55	,	'PIECE'	,	1.24 / (1000 / 150)	),
(	56	,	'PIECE'	,	1.24 / (1000 / 150)	),
(	57	,	'PIECE'	,	1.24 / (1000 / 150)	),
(	58	,	'PIECE'	,	1.24 / (1000 / 150)	),
(	59	,	'PIECE'	,	2.7 / (1000 / 150)	),
(	60	,	'PIECE'	,	2.7 / (1000 / 150)	),
(	61	,	'PIECE'	,	1.24 / (1000 / 150)	),
(	62	,	'PIECE'	,	1.24 / (1000 / 150)	),
(	63	,	'PIECE'	,	1.24 / (1000 / 150)	),
(	64	,	'KG'	,	2.74	),
(	65	,	'KG'	,	6.78	),
(	66	,	'KG'	,	6.5	),
(	67	,	'KG'	,	5.5	),
(	68	,	'KG'	,	7.5	),
(	69	,	'PIECE'	,	0.74	)
ON DUPLICATE KEY UPDATE unit_product=VALUES(unit_product), price_product=VALUES(price_product);

INSERT INTO recipe VALUES
(1, "", 120, "https://static.cuisineaz.com/400x320/i130801-pot-au-feu-au-cookeo.jpeg", "Pot-au-feu", "RECIPE", NULL),
(2, "", 0, "https://images-ca-1-0-1-eu.s3-eu-west-1.amazonaws.com/photos/slide/262/bouquet-garni-3000x2000.jpg", "Bouquet garni", "RECIPE", NULL),
(3, "", 30, "https://static.cuisineaz.com/400x320/i56611-ratatouille-nicoise.jpeg", "Ratatouille", "RECIPE", NULL),
(4, "", 45, "https://static.cuisineaz.com/400x320/i138202-minestrone-facon-paysanne.jpeg", "Minestrone paysan", "RECIPE", NULL),
(5, "", 0, "https://static.750g.com/images/600-400/e4fd22a6cc2d18daf8371f20aad9ad39/thinkstockphotos-592398608.jpg", "Découverte pommes", "RECIPE", NULL);


INSERT INTO type_has_recipe (recipe_id_recipe, type_id_type, quantity) VALUES
(1, 44, 4),
(1, 8, 4),
(1, 10, 1),
(1, 39, 2),
(1, 2, 1),
(2, 43, 0.05),
(2, 68, 0.05),
(2, 30, 0.05),
(3, 16, 2),
(3, 5, 1),
(3, 47, 1),
(3, 46, 1),
(3, 69, 3),
(3, 39, 1),
(3, 2, 2),
(4, 39, 1),
(4, 69, 2),
(4, 16, 1),
(4, 8, 1),
(4, 10, 2),
(4, 64, 0.06),
(4, 2, 1),
(4, 26, 0.2),
(5, 48, 1),
(5, 49, 1),
(5, 50, 1),
(5, 51, 1),
(5, 52, 1),
(5, 53, 1),
(5, 54, 1),
(5, 55, 1),
(5, 56, 1),
(5, 57, 1),
(5, 58, 1),
(5, 59, 1),
(5, 60, 1),
(5, 61, 1);


insert into ordered_recipe values("2020-02-10", 1, 1);


INSERT INTO market_day (id_market_day, market_id_market, producer_id_producer, day_market_day) VALUES
(1,  1, 1, 'MONDAY'),
(2,  1, 1, 'SATURDAY'), 
(3,  1, 1, 'SUNDAY'),
(4,  2, 2, 'SATURDAY'), 
(5,  2, 2, 'SUNDAY'),
(6,  3, 3, 'SATURDAY'), 
(7,  4, 4, 'TUESDAY'),
(8,  4, 4, 'SATURDAY'), 
(9,  4, 4, 'SUNDAY'),
(10, 5, 5, 'SATURDAY'), 
(11, 5, 5, 'SATURDAY'), 
(12, 7, 6, 'WEDNESDAY'), 
(13, 7, 6, 'FRIDAY'),
(14, 7, 6, 'SATURDAY'), 
(15, 7, 6, 'SUNDAY');